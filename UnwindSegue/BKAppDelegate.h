//
//  BKAppDelegate.h
//  UnwindSegue
//
//  Created by Will Dampney on 1/01/14.
//  Copyright (c) 2014 Brasskazoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
