//
//  BKViewController.m
//  UnwindSegue
//
//  Created by Will Dampney on 1/01/14.
//  Copyright (c) 2014 Brasskazoo. All rights reserved.
//

#import "BKViewController.h"

@interface BKViewController ()

@end

@implementation BKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindSegue:(UIStoryboardSegue *)segue {
    // This method enables the unwinding of the segue, and is called prior to the unwind action.
    NSLog(@"Unwinding segue %@", segue.identifier);
    
    // The unwind could also be called from other code using the unwind identifier:
    // [self performSegueWithIdentifier:@"UnwindToFirstView" sender:self];
}

@end
