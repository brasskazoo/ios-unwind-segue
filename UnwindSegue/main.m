//
//  main.m
//  UnwindSegue
//
//  Created by Will Dampney on 1/01/14.
//  Copyright (c) 2014 Brasskazoo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BKAppDelegate class]));
    }
}
